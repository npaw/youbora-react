# react-youbora-app

> react-youbora-app

[![NPM](https://img.shields.io/npm/v/react-youbora-app.svg)](https://www.npmjs.com/package/react-youbora-app) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-youbora-app
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-youbora-app'
import 'react-youbora-app/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [hcorta](https://github.com/hcorta)
