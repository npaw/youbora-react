import React from 'react'
import styles from './styles.module.css'

export class YouboraApp extends React.Component {
  constructor(props) {
    super(props)
    this.privateUrl = 'app'
    this.baseUrl = 'newApp'
    this.exactRoute = true
    this.id = Math.random()
    this.label = 'app'
    this.owner = 'YOUBORA'
    this.icon = () => <span>Your icon</span>
    this.permission = ''
    this.theme = null
    this.appTree = {}
    // this.store = create Store(reducer)
  }

  // componentDidMount() {
  //   const { name, host } = this.props
  //   const scriptId = `micro-frontend-script-${name}`

  //   if (document.getElementById(scriptId)) {
  //     this.renderMicroFrontend()
  //     return
  //   }

  //   fetch(`${host}/asset-manifest.json`)
  //     .then((res) => res.json())
  //     .then((manifest) => {
  //       const script = document.createElement('script')
  //       script.id = scriptId
  //       script.src = `${host}${manifest['main.js']}`
  //       script.onload = this.renderMicroFrontend
  //       document.head.appendChild(script)
  //     })
  // }

  // componentWillUnmount() {
  //   const { name } = this.props

  //   window[`unmount${name}`](`${name}-container`)
  // }

  // renderMicroFrontend = () => {
  //   const { name, history } = this.props

  //   window[`render${name}`](`${name}-container`, history)
  //   // E.g.: window.renderBrowse('browse-container', history);
  // }

  render() {
    return <div>This is your new YOUBORA App</div>
  }
}
